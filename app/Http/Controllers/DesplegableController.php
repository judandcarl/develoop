<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB as DB;
use App\Pais as Pais;
use App\Tipologia as Tipologias;

class DesplegableController extends Controller
{
  public function pais()
    {
       $pais = DB::table('pais')->orderBy('pais_name', 'asc')->get();
       return response()->json($pais);
    }

    public function tipologia()
      {
         $tipo = DB::table('tipologia')->orderBy('tip_name', 'asc')->get();
         return response()->json($tipo);
      }
}
