<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB as DB;
use App\User as User;
use App\Empresa as Empresas;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('empresas.registro')->with('estatus', 'creando');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

      try {
        $user = new User;

        $user->email=$request->email;
        $user->password=$request->password;

        if($user->save()){

          $empresa = new Empresas;

          $empresa->emp_name=$request->name;
          $empresa->emp_estado=$request->estado;
          $empresa->emp_ciudad=$request->ciudad;
          $empresa->emp_observaciones=$request->observaciones;
          $empresa->pais_id=$request->pais;
          $empresa->tip_id=$request->tipologia;
          $empresa->user_id=$user->id;

          if($empresa->save()){
            return redirect('listado_empresas')->with('estatus', 'ok');
          }//fin del if $empresa

        }//fin del if $user

      }//fin del try
       catch (Exception $e) {
         return redirect('empresa')->with('estatus', 'null');
      }//fin del catch

    }//fin de la funcion create

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $empresa= DB::table('empresa')
      ->join('pais', 'empresa.pais_id', '=', 'pais.pais_id')
      ->join('tipologia', 'empresa.tip_id', '=', 'tipologia.tip_id')
      ->select('empresa.*','pais.pais_name', 'tipologia.tip_name')->get();

      return view('empresas.lista')->with('empresa', $empresa);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empresa = Empresas::where('emp_id',$id)->first();
        return response()->json($empresa);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
          Empresas::where('emp_id',  $id)
          ->update([
              'emp_name' => $request->name,
              'emp_estado' => $request->estado,
              'emp_ciudad' => $request->ciudad,
              'emp_observaciones' => $request->observaciones,
              'pais_id' => $request->pais,
              'tip_id' => $request->tipologia
          ]);
          return response()->json('ok');
        }catch(\Exception $e){
            return response()->json($e);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
