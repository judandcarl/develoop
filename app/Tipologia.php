<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipologia extends Model
{
    protected $table = 'tipologia';

    protected $primaryKey = 'tip_id';

}
