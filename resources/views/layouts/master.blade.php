<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>DeveLoop</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap/css/bootstrap.css" rel="stylesheet">

    <style>
    .panel-footer{
      background-color: #fff;
    }
    .bg-white{
      background-color: #fff;
    }
    .navbar-inverse{
      border:none;
    }
    .menu:hover{
      color: #2295a1 !important;
    }
    .pipe{
      padding-top: 15px;
    padding-bottom: 15px;
    }
    .bg-green{
      background-color: #2295a1;
    }
    .btn-green{
      background-color: #2295a1;
      border-color: #2295a1;
      color:#fff;
    }
    .navbar-toggle {
      float:left;
      background-color: #2295a1;
      border:none;
    }
    </style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

      <div class="container">

        <div class="row">
          <nav class="navbar navbar-inverse">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>
              <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                  <li><a class="menu" href="{{"/"}}">Inicio</a></li>

                  <li class="dropdown active">
                         <a class="dropdown-toggle" data-toggle="dropdown" href="#">Empresa
                         <span class="caret"></span></a>
                         <ul class="dropdown-menu">
                           <li><a href="{{'empresa'}}">Registro de Empresas</a></li>
                           <li><a href="{{'listado_empresas'}}">Listado de Empresas</a></li>
                         </ul>
                  </li>

                  <li><a class="menu" href="#">Servicios</a></li>
                  <li><a class="menu" href="#">Valora un Servicio</a></li>
                  <li><a class="menu" href="#">Contacto</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right bg-green">
                  <li><a href="#"> Registro </a></li>
                  <li><a href="#">Iniciar Sesión</a></li>
                </ul>
              </div>
            </div>
          </nav>

        </div>


        <div class="row">
           @yield('contenido')
        </div>
        <div class="row">
          <div class=" panel panel-footer"><!--inicio del footer -->
            <div class="row">
              <div class="col-md-2"></div>
              <div class="col-md-10">
                <div class="row">
                  <div class="col-md-3">
                    <h4>Empresas</h4>
                    <h6>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h6>
                  </div>

                  <div class="col-md-3">
                    <h4>Servicios</h4>
                    <h6>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h6>
                  </div>

                  <div class="col-md-3">
                    <h4>Valora un servicio</h4>
                    <h6>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h6>
                  </div>

                  <div class="col-md-3">
                    <h4>Contacto</h4>
                    <h6>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h6>
                  </div>
                </div>
                <div class="row top">
                  <div class="col-md-11">
                      <p><b>Opiniones y consejos sobre servicios sanitarios, funerarios y muchos más.</b>
                        <br>
                        ©2017 Todos los derechos reservados.<u>Condiciones de uso</u>, <u>Política privacidad</u> y <u>Política de cookies.</u>
                        <br>
                        <br>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.
                      </p>
                  </div>
                </div>
              </div>
            </div>



            <h5 align="center">Veritus©2014</h5>
          </div><!--Fin del footer -->
        </div>
      </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="css/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js" type="text/javascript"></script>
     @yield('script')
  </body>
</html>
