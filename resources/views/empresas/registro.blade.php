@extends('layouts.master')
@section('contenido')

<p><i>Estás en: Inicio > Empresas</i> > <b>Registro de Empresas</b> </p>
<div class=" panel panel-footer">
  <div class="panel-body">

        <form action="{{URL('registro')}}" method="POST" id="form">
          {{ csrf_field() }}
          <div class="row">

            <div class="col-xs-4">
              <h4>REGISTRO DE EMPRESAS</h4>
            </div>
            <div class="col-xs-8">
              <div class="pull-right">
                <button type="submit" class="btn btn-green" name="button">Guardar Cambios</button>
              </div>
            </div>
            <hr></div>
          <div class="row">
            <div class="col-md-12">
               <h4>Información Básica</h4>
            </div>
          </div>
          <div class="row">
            <div class="col-md-7">
              <div class="row">
                <div class="clearfix"></div>
                <div class="jumbotron"><!--Inicio del jumbotron -->
                <div class="col-md-6">
                  <div class="form-group ">
                    <label for="ejemplo_email_1">Nombre de la Empresa</label>
                    <input class="form-control" type="text" name="name" required pattern="[A-Za-z]{3,15}" maxlength="15" placeholder="nombre de la empresa">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group ">
                    <label  for="ejemplo_email_1">Tipología de la Empresa</label>
                        <select class="form-control" id="tipologia" name="tipologia" required>
                          <option value="" disabled selected>Seleccione</option>
                        </select>
                  </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-4">
                  <div class="form-group ">
                    <label for="ejemplo_email_1">País:</label>
                        <select class="form-control" id="pais" name="pais" required>
                          <option value="" disabled selected>Seleccione</option>
                        </select>
                  </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group ">
                      <label for="ejemplo_email_1">Estado</label>
                      <input class="form-control" type="text" name="estado" required pattern="[A-Za-z ]{3,15}" maxlength="15" placeholder="Ingrese Estado">
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group ">
                    <label for="ejemplo_email_1">Ciudad</label>
                    <input class="form-control" type="text" name="ciudad" required pattern="[A-Za-z ]{3,15}" maxlength="15" placeholder="Ingrese Ciudad">
                  </div>
                </div>
                <div class="clearfix"></div>
               </div><!--fin del jumbotron -->

              </div>
              <div class="row">
                <div class="col-md-12">
                  <h4>Información Registro</h4>
                </div>
                <div class="clearfix"></div>
                <div class="jumbotron"><!--inicio del jumbotron 2 -->
                  <div class="col-md-4">
                    <div class="form-group ">
                      <label for="ejemplo_email_1">Email/Usuario</label>
                      <input type="text" class="form-control" name="email" id="ejemplo_password_3" placeholder="Ingrese su Correo"  pattern="[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" maxlength="30"  required>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group ">
                      <label for="ejemplo_email_1">Password</label>
                      <input class="form-control" placeholder="Coloque su Clave" type="password" name="password" pattern="[a-zA-Z0-9]{6,30}"  maxlength="30" required>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group ">
                      <label for="ejemplo_email_1">Repetir Password</label>
                      <input class="form-control" placeholder="Repita su Clave" type="password" name="password2" pattern="[a-zA-Z0-9]{6,30}"  maxlength="30" required>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div><!--fin del jumbotron 2 -->

              </div> </div>
            <div class="col-md-5">
                <div class="jumbotron"><!--inicio del jumbotron 3 -->
                  <div class="form-group">
                    <label for="ejemplo_email_1">Observación</label>
                    <textarea class="form-control" rows="17" name="observaciones" required></textarea>
                  </div>

                </div><!--fin del jumbotron 3 -->
            </div>
         </div>
        </form>
  </div>
</div>

@endsection
@section('script')
<script type="text/javascript">

//inicio del desplegable pais
$.get( "{{URL('/pais')}}",function(data){
for(var i=0; i<data.length; i++){
$("#pais").append('<option value="'+data[i].pais_id+'">'+data[i].pais_name+'</option>');
}
} );//Fin del desplegable pais

//inicio del desplegable pais
$.get( "{{URL('/tipologia')}}",function(data){
for(var i=0; i<data.length; i++){
$("#tipologia").append('<option value="'+data[i].tip_id+'">'+data[i].tip_name+'</option>');
}
} );//Fin del desplegable pais

//Inicio de Validacion de contraseñas
$( document ).ready(function() {
$("#form").submit(function(e){

  if (this.password.value!=this.password2.value) {
    alert('Las claves no coinciden');
    return false;

  }else {
    return true;
  }

});
});//fin de validacion de contraseñas

</script>
@endsection
