@extends('layouts.master')
@section('contenido')
<p><i> Estás en: Inicio > Empresas</i> > <b>Listado de Empresas</b> </p>
<div class=" panel panel-footer">
  <div class="panel-body">
<h4>Listado de Funcionarios Y Empleados Registrados</h4>
<br>
<div class="">


<!--inicio de la tabla-->
<div class="table-responsive">
<table id="tables" class="table">
    <thead >
        <tr>
            <th>Nombre </th>
            <th>Tipología</th>
            <th>País</th>
            <th>Ciudad</th>
            <th>Estado</th>
            <th>Observaciones</th>
            <th>Editar</th>

        </tr>
    </thead>
<tbody>
@foreach ($empresa as $empresas)
        <tr>
            <td>{{ $empresas->emp_name }}</td>
            <td>{{ $empresas->tip_name }}</td>
            <td>{{ $empresas->pais_name }}</td>
            <td>{{ $empresas->emp_ciudad }}</td>
            <td>{{ $empresas->emp_estado }}</td>
            <td>{{ $empresas->emp_observaciones }}</td>
            <td><button value="{{ $empresas->emp_id}}" class="btn btn-info btn-lg myBtn" id="myBtn"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button> </td>

        </tr>
@endforeach
</tbody>
</table><!--fin de la tabla-->
</div>
</div>
</div>
</div>

<!-- Inicio del Modal -->
<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Datos de la Empresa</h4>
        </div>

        <div class="modal-body">
         <form id="form">
          {{ csrf_field() }}
              <div class="row">
                <div class="col-md-12">
                <div class="form-group ">
                  <label for="">Nombre de la Empresa</label>
                  <input class="form-control" type="text" name="name" id="emp" required pattern="[A-Za-z]{3,15}" maxlength="15" placeholder="nombre de la empresa">
                </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label  for="ejemplo_email_1">Tipología de la Empresa</label>
                      <select class="form-control" id="tipologia" name="tipologia" required>
                        <option value="" disabled selected>Seleccione</option>
                      </select>
                </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-12">
                <div class="form-group ">
                  <label for="ejemplo_email_1">País:</label>
                      <select class="form-control" id="pais" name="pais" required>
                        <option value="" disabled selected>Seleccione</option>
                      </select>
                </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-12">
                <div class="form-group ">
                  <label for="">Estado</label>
                  <input class="form-control" type="text" name="estado" id="est" required pattern="[A-Za-z ]{3,15}" maxlength="15" placeholder="Ingrese Estado">
                </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-12">
                <div class="form-group ">
                  <label for="">Ciudad</label>
                  <input class="form-control" type="text" name="ciudad" id="cit" required pattern="[A-Za-z ]{3,15}" maxlength="15" placeholder="Ingrese Ciudad">
                </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-12">
                <div class="form-group ">
                  <label for="ejemplo_email_1">Observación</label>
                  <textarea class="form-control" rows="3" name="observaciones" id="obs" required></textarea>
                </div>
              </div>
              </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
          <button type="submit" id="save" class="btn btn-primary" data-dismiss="modal">Guardar</button>
        </div>
      </div>

    </div>
  </div>

</div>

<!-- Fin del Modal -->



@endsection

@section('script')


        <script type="text/javascript">
        var value;
            $(document).ready( function () {
                //script que Inicia el DataTable
                $('#tables').DataTable({
                  responsive: true
                });

                  //inicio del desplegable pais
                  $.get( "{{URL('/pais')}}",function(data){
                  for(var i=0; i< data.length; i++){
                  $("#pais").append('<option value="'+data[i].pais_id+'">'+data[i].pais_name+'</option>');
                  }
                  } );//Fin del desplegable pais

                  //inicio del desplegable pais
                  $.get( "{{URL('/tipologia')}}",function(data){
                  for(var i=0; i <data.length; i++){
                  $("#tipologia").append('<option value="'+data[i].tip_id+'">'+data[i].tip_name+'</option>');
                  }
                  } );//Fin del desplegable pais
                //script que Inicia el Modal
                $(".myBtn").click(function(){
                  value = this.value;
                  console.log(value)
                  $.get('/get_empresa/' + value)
                  .then(getEmpresa);
                });
                $("#save").click(function(){
                  var data=$("#form").serializeArray();
                  $.ajax({
                    url: "/actualizar/" + value,
                    data: data,
                    method: "PUT",
                    success:onSuccess
                  });
                })
                function onSuccess(res){
                  if(res === 'ok'){
                    window.location.href = "{{URL('/listado_empresas')}}";
                  }
                }
                function getEmpresa (response) {
                  console.log(response)
                  $("#emp").val(response.emp_name);
                  $("#tipologia").val(response.tip_id);
                  $("#pais").val(response.pais_id);
                  $("#est").val(response.emp_estado);
                  $("#cit").val(response.emp_ciudad);
                  $("#obs").val(response.emp_observaciones);
                  $("#myModal").modal();
                }
            });
        </script>
        @endsection
