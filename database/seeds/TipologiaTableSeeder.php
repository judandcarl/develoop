<?php

use Illuminate\Database\Seeder;

class TipologiaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $tipos=[
        'Compañía Anónima',

        'Sociedad Anónima',

        'Sociedad Limitada',

        'Sociedad limitada de nueva creación',

        'Sociedad de Responsabilidad Limitada',

        'Sociedad Colectiva',

        'Sociedad Cooperativa y Agrupaciones de interés económico',

        'Sociedad Comanditaria',

        'Sociedad Comanditaria por acciones',

        'Sociedad Laboral',

        'Asociaciones sin ánimo de lucro'
       ];

   foreach ($tipos as $tipo) {

       DB::table('tipologia')->insert([
         'tip_name' =>$tipo,
         'created_at' => date("Y-m-d H:i:s"),
         'updated_at' => date("Y-m-d H:i:s")
       ]);
      }
    }
}
