<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

  Route::get('/empresa', 'EmpresaController@index');
  Route::get('/get_empresa/{id}', 'EmpresaController@show');
  Route::post('/registro', 'EmpresaController@create');
  Route::get('/pais', 'DesplegableController@pais');
  Route::get('/tipologia', 'DesplegableController@tipologia');
  Route::get('/listado_empresas', 'EmpresaController@store');
  Route::put('/actualizar/{id}', 'EmpresaController@update');
